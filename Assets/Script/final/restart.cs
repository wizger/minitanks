﻿/*Autor: Gerson Ivan Perez Dominguez
	12002764*/
using UnityEngine;
using System.Collections;

public class restart : MonoBehaviour {
	Camera camera;
	public Color color1 = Color.red;
	public Color color2 = Color.blue;
	public float duration = 0.000000300F;
	float vM,vE;
	string estado;
	coorM cm;
	coorEne ce;
	// Use this for initialization
	void Start () {
		cm = new coorM ();
		ce = new coorEne ();
		vM = cm.getVida ();
		vE = ce.getVida ();
		cm.restart ();
		ce.restart ();
		estado = "";
		camera = GetComponent<Camera>();
		camera.clearFlags = CameraClearFlags.SolidColor;
	}
	void OnGUI(){
		if (vM <= 0) {
			estado = "PERDIO";
		} else {
			estado = "GANO";
		}
		GUI.Label (new Rect ((Screen.width / 2) - 100, ((Screen.height / 2) - 100) + 10, 200, 50), estado);

		if (GUI.Button (new Rect ((Screen.width/2)-100,(Screen.height/2)-25,200, 50), "reiniciar")){
			Application.LoadLevel("Menu");
		} 
	}

	// Update is called once per frame
	void Update () {
		float t = Mathf.PingPong(Time.time, duration) / duration;
		camera.backgroundColor = Color.Lerp(color1, color2, t);
		//camera.backgroundColor = color1;

	}
}
