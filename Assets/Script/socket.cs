/*Autor: Gerson Ivan Perez Dominguez y Rodrigo Coronado
 12002764 12002114*/
using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading;
using System;
using System.IO;
using System.Text.RegularExpressions;

public class socket : MonoBehaviour {
	private AudioSource disparo,sonidoSource;
	public AudioClip sonido,sonidoMenu;
	public GameObject particulas,imgFon;
	public Sprite local,invitado;
	coorEne coor;
	coorM cM;
	public Camera cam;
	TcpListener tcpListener;
	int sizeX,sizeY,screenX,screenY;
	float tanqueX,tanqueY;
	public Transform fondo,tanque,tanqueE,casa;
	string ip;
	comunicacion com,speak;
	Thread comuThread,speakT;
	bool conectado,juego;
	public Renderer tamanoTan;
	// Use this for initialization
	void Start () {
		tcpListener = null;
		tanqueX = ((tamanoTan.bounds.size.x* 10f)/2f) + 2f;
		tanqueY = ((tamanoTan.bounds.size.y * 10f)) + 30f;
		screenX = Screen.width/2;
		screenY = Screen.height/2;
		sizeX = 200;	
		sizeY = 50;
		coor = new coorEne ();
		ip = "";
		conectado = juego = false;
		cM = new coorM ();
		coor.initVida (10);
		cM.initVida (10);

		//Instantiate (fondo,cam.ScreenToWorldPoint(new Vector3(screenX + 0f,screenY+0f,10f)),Quaternion.identity);
		/*tanque.GetComponent<SpriteRenderer>().sprite = local;
		Instantiate (tanque,cam.ScreenToWorldPoint(new Vector3(tanqueX * tanqueX,(screenY*2) - tanqueY+0f,10f)),Quaternion.identity);
		tanque.GetComponent<SpriteRenderer>().sprite = local;
		Instantiate (tanqueE,cam.ScreenToWorldPoint(new Vector3(10f,300f,10f)),Quaternion.identity);*/

		cM.setShoot (true);
		cM.setSShoot (false);
		coor.setSShoot (false);
		disparo = GetComponent<AudioSource>();
		sonidoSource = GetComponent<AudioSource>();
		sonidoSource.PlayOneShot (sonidoMenu, 1);
	}

	void OnGUI(){
		if (conectado) {

			if (comuThread.IsAlive) {
				if(juego){
						
				}else{
					if(com.isReady()){
						if(com.isJefe()){
							tanque.GetComponent<SpriteRenderer>().sprite = local;
							tanqueE.GetComponent<SpriteRenderer>().sprite = invitado;
							Instantiate (tanque,cam.ScreenToWorldPoint(new Vector3(tanqueX * tanqueX,(screenY*2) - tanqueY+0f,10f)),Quaternion.identity);
							Instantiate (tanqueE,cam.ScreenToWorldPoint(new Vector3(screenX*2 - (tanqueX*tanqueX),tanqueY,10f)),Quaternion.identity);
							Vector3 ce = cam.ScreenToWorldPoint(new Vector3(screenX*2 - (tanqueX*tanqueX),tanqueY,10f));
							Vector3 cm = cam.ScreenToWorldPoint(new Vector3(tanqueX * tanqueX,(screenY*2) - tanqueY+0f,10f));
							coor.setXY(ce.x,ce.y);
							coor.setAngle(0f);
							cM.setXY(cm.x,cm.y);
							cM.setAngle(0f);
						}else{
							tanque.GetComponent<SpriteRenderer>().sprite = invitado;
							tanqueE.GetComponent<SpriteRenderer>().sprite = local;
							Instantiate (tanque,cam.ScreenToWorldPoint(new Vector3(screenX*2 - (tanqueX*tanqueX),tanqueY,10f)),Quaternion.identity);
							Instantiate (tanqueE,cam.ScreenToWorldPoint(new Vector3(tanqueX * tanqueX,(screenY*2) - tanqueY+0f,10f)),Quaternion.identity);
							Vector3 ce = cam.ScreenToWorldPoint(new Vector3(tanqueX * tanqueX,(screenY*2) - tanqueY+0f,10f));
							Vector3 cm = cam.ScreenToWorldPoint(new Vector3(screenX*2 - (tanqueX*tanqueX),tanqueY,10f));
							coor.setXY(ce.x,ce.y);
							cM.setXY(cm.x,cm.y);
						}
						/*
						 Instantiate (tanque,cam.ScreenToWorldPoint(new Vector3(screenX - com.getX()+0f,screenY - com.getY()+0f,10f)),Quaternion.identity);
						Instantiate (tanqueE,cam.ScreenToWorldPoint(new Vector3(screenX - coor.getX()+0f,screenY - coor.getY ()+0f,10f)),Quaternion.identity);*/

						//casas izquierda
						Instantiate (casa,cam.ScreenToWorldPoint(new Vector3(64,170,10f)),Quaternion.identity);
						Instantiate (casa,cam.ScreenToWorldPoint(new Vector3(200,360,10f)),Quaternion.identity);
						Instantiate (casa,cam.ScreenToWorldPoint(new Vector3(64,550,10f)),Quaternion.identity);

						//casas en medio
						Instantiate (casa,cam.ScreenToWorldPoint(new Vector3(screenX,screenY,10f)),Quaternion.identity);
						Instantiate (casa,cam.ScreenToWorldPoint(new Vector3(screenX -164,170,10f)),Quaternion.identity);
						Instantiate (casa,cam.ScreenToWorldPoint(new Vector3(screenX +134,550,10f)),Quaternion.identity);
						//casas derecha
						Instantiate (casa,cam.ScreenToWorldPoint(new Vector3((screenX*2)-64,170,10f)),Quaternion.identity);
						Instantiate (casa,cam.ScreenToWorldPoint(new Vector3((screenX*2)-264,360,10f)),Quaternion.identity);
						Instantiate (casa,cam.ScreenToWorldPoint(new Vector3((screenX*2)-64,550,10f)),Quaternion.identity);


						Instantiate (fondo,cam.ScreenToWorldPoint(new Vector3(screenX + 0f,screenY+0f,10f)),Quaternion.identity);
						Destroy(particulas);
						Destroy(imgFon);
						juego = true;
						com.readyOff();
						sonidoSource.Stop();
						
					}
					//label que muestra la cantidad de usuarios conectado
					GUI.Label (new Rect ((Screen.width / 2) - (sizeX / 2), ((Screen.height / 2) - (sizeY / 2)) + 10, sizeX, sizeY), "Conectados:" + com.getNumJuga ());
					if(com.isJefe()){
						if(Int32.Parse(com.getNumJuga()) ==2){
							if (GUI.Button (new Rect ((Screen.width / 2) - (sizeX / 2), ((Screen.height / 2) - (sizeY / 2)) + 30, sizeX, sizeY), "Empezar")) {
								//aqui va a empezar el juego
								com.setReady();
							}
						}
					}
					if (GUI.Button (new Rect ((Screen.width / 2) - (sizeX / 2), ((Screen.height / 2) - (sizeY / 2)) + 90, sizeX, sizeY), "Cancelar")) {
						closeAllThreads();
						disparo.PlayOneShot (sonido, 1);
					}
				}
			}
		} else {
			ip = GUI.TextField(new Rect ((Screen.width / 2) - (sizeX / 2), ((Screen.height / 2) - (sizeY / 2)) - 50, sizeX, sizeY),ip);
			if (GUI.Button (new Rect ((Screen.width / 2) - (sizeX / 2), ((Screen.height / 2) - (sizeY / 2)) + 10, sizeX, sizeY), "Conectar")){
				//opcion = 0;//aqui va a estar la parte de conectar al server :D
				startAllThreads();
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	}







	void OnApplicationQuit() {
		closeAllThreads ();
	}
	void startAllThreads(){
		com = new comunicacion(ip);
		com.setName("listener");
		comuThread = new Thread(com.doWork);
		comuThread.Start();
		speak = new comunicacion(ip);
		speak.setName("speaker");
		speakT = new Thread(speak.doWork);
		speakT.Start();
		conectado = true;
		disparo.PlayOneShot (sonido, 1);
	}
	void closeAllThreads(){
		com.stopRun ();
		com.printToSocket("close");
		comuThread.Abort();
		speak.stopRun ();
		speak.printToSocket("close");
		speakT.Abort();
		conectado = false;
	}
}

public class comunicacion{
	static bool jefe,ready,shootOnce;
	static int estado;
	static float X,Y;
	coorM coM;
	float OldX, oldY;
	coorEne ce;
	int oldLife;
	IPEndPoint ip;
	Socket server;
	int puerto,b,receivedDataLength;
	bool doWhile,sendCoorFirst;
	byte[] data;
	String stringData,iptxt,numJugadores,name;
	coorEne coor;
	public comunicacion(String iptxt){
		this.iptxt = iptxt;
		estado = 1;
		coM = new coorM ();
		data = new byte[2024];
		shootOnce = true;
		coor = new coorEne ();
		ce = new coorEne ();
		oldLife = ce.getVida();
		b = 0;
		X = Y = 0f;
		name = "";
		jefe = ready = false;
		sendCoorFirst = true;
		numJugadores = "0";
		stringData = "";
			puerto = 8002;
		doWhile = false;
		ip = new IPEndPoint(IPAddress.Parse(iptxt), puerto);
		server = new Socket(AddressFamily.InterNetwork,SocketType.Stream, ProtocolType.Tcp);
		try
		{
			server.Connect(ip);
			doWhile=true;
		} catch (SocketException e){
			Console.WriteLine("Unable to connect to server.");
		}
		
	}


	//para imprimir en pantalla usar Debug.Log()
	/*
	 * rd : se manda esto cuando el thread jefe le da en empezar juego
	 */
	public void doWork(){
		coM.setMove (true);
		while (doWhile) {
			if(name.Equals("speaker")){
				if(estado == 2){
					sendCom();
				}
			}else if (name.Equals("listener")){
				listenFromSocket();

				try{
					if(!stringData.Equals("")){
						if(estado ==1){
							comandos ();
						}else if(estado ==2){
							//comandos2();
							if(stringData.Length > 5){
								parserCord(stringData);
							}
						}
					}
				}catch(Exception e){}
			}
		}
		
	}
	//este metodo se llama luego que escucha para poder ver que comando se le mando
	void comandos(){
		if(stringData.Substring(0,2).Equals("cj")){
			setNumJuga(stringData.Substring(2));
		}else if(stringData.Substring(0,2).Equals("jf")){
			setJefe(stringData.Substring(2));
		}else if(getString (stringData,@"rd").Equals("rd")){
			ready = true;
			estado = 2;
		}else if(stringData.Substring(0,2).Equals("ex")){
			coor.setX((float)getNum());
		}else if(stringData.Substring(0,2).Equals("ey")){
			coor.setY((float)getNum());	
		}else if(stringData.Substring(0,2).Equals("cx")){
			X  = (float)(getNum());	
		}else if(stringData.Substring(0,2).Equals("cy")){
			Y  = (float)(getNum());	
		}
	}

	//cuando estado = 2 este escucha
	void comandos2(){
		//Debug.Log (stringData);
		if (stringData.Substring (0, 2).Equals ("cx")) {
			ce.setX (getCoor ());
		} else if (stringData.Substring (0, 2).Equals ("cy")) {
			ce.setY (getCoor ());
		} else if (stringData.Substring (0, 2).Equals ("mv")) {
			coM.setMove (true);
		} else if (stringData.Substring (0, 2).Equals ("an")) {
			ce.setAngle (getCoor ());
		} else if (stringData.Substring (0, 2).Equals ("bx")) {
			ce.setXB (getCoor ());
		} else if (stringData.Substring (0, 2).Equals ("by")) {
			ce.setYB (getCoor ());
			if (shootOnce) {
				ce.setSShoot (true);
				shootOnce = false;
			}
		} else if (stringData.Substring (0, 2).Equals ("en")) {
			shootOnce = true;
		} else if (stringData.Substring (0, 2).Equals ("cv")) {
			if(ce.getVida() != oldLife){
				ce.initVida (oldLife-5);
				oldLife = ce.getVida();
			}
		}
	}
	/*cxnum y cynum estos se envian para mandar mis coordenadas
	 */
	void sendCom(){
		//Debug.Log ("entro");
		String info = "";
		if (sendCoorFirst) {
			if(isJefe()){
				info = "j"+coM.getX() + "@"+coM.getY() + "@"+coM.getAngle() + "@"+coM.getVida();
				if(coM.canSShoot()){
					info += "@"+coM.getXB() + "@"+coM.getYB();
				}else{
					info += "@je";
				}
			}else{
				info = coM.getX() + "@"+coM.getY() + "@"+coM.getAngle() + "@"+coM.getVida();
				if(coM.canSShoot()){
					info += "@"+coM.getXB() + "@"+coM.getYB();
				}else{
					info += "@je";
				}
			}
			sendCoorFirst = true;//false
			if ((ce.getVida () <= 0) || (coM.getVida () <= 0)) {
				printToSocket("fn");
				jefe = ready = doWhile =false;
				shootOnce = true;
				estado = 1;
				X = Y = 0f;
				//matar thead
			}else{
				printToSocket(info);
			}
		}
		//aqui va ir un else para no mandar pos a cada rato

		Thread.Sleep(15);
	}

	public int getNum(){
		string pattern = @"-*[0-9]+";
		Match m = Regex.Match(stringData, pattern);
		if(m.Success){
			return Int32.Parse(m.Value);
		}
		return -1;
	}

	public float getCoor(){
		string pattern = @"-*[0-9]+\.[0-9]+";
		Match m = Regex.Match(stringData, pattern);
		if(m.Success){
			return float.Parse(m.Value);
		}
		return 0f;
	}

	public String getString(String txt,String pat){
		Match m = Regex.Match(txt, pat);
		if(m.Success){
			return m.Value;
		}
		return "";
	}

	public void printToSocket(String txt){
		server.Send(Encoding.ASCII.GetBytes(txt+"\n"));
	}

	public void listenFromSocket(){
		Array.Clear (data,0,data.Length);
		receivedDataLength = server.Receive(data);
		stringData = Encoding.ASCII.GetString(data, 0, receivedDataLength);
	}
	//le pone nombre al thread
	public void setName(String name){
		this.name = name;
	}

	public void stopRun(){
		doWhile = false;
	}

	public void setNumJuga(String numJugadores){
		this.numJugadores = numJugadores;
	}

	public String getNumJuga(){
		return numJugadores;
	}
	

	public void setJefe(String txt){
		if (Int32.Parse (txt) == 0) {
			jefe = true;
		} else {
			jefe = false;
		}
		ce.setJefe (jefe);
	}

	public bool isJefe(){
		return jefe;
	}
	//este metodo le dice al servidor que ya va a empezar el juego
	public void setReady(){
		printToSocket ("rd");
	}

	public bool isReady(){
		return ready;
	}

	public void readyOff(){
		ready = false;
	}

	public float getX(){return X;}
	public float getY(){return Y;}
	public void setXY(float x1,float y1){
		X = x1;
		Y = y1;
	}



	void parserCord(String aux){
		aux = aux.Substring(0,aux.IndexOf("\n"));
		int num = contador (aux);
		//Debug.Log(num);
		if (num == 5) {
			float corX = float.Parse (aux.Substring (0, aux.IndexOf ("@")));
			aux = aux.Substring ((aux.IndexOf ("@")) + 1, aux.Length - (aux.IndexOf ("@") + 1));
			ce.setX (corX);
			
			
			float corY = float.Parse (aux.Substring (0, aux.IndexOf ("@")));
			aux = aux.Substring ((aux.IndexOf ("@")) + 1, aux.Length - (aux.IndexOf ("@") + 1));
			ce.setY (corY);
			
			
			float corA = float.Parse (aux.Substring (0, aux.IndexOf ("@")));
			aux = aux.Substring ((aux.IndexOf ("@")) + 1, aux.Length - (aux.IndexOf ("@") + 1));
			ce.setAngle(corA);
			
			
			float life = float.Parse (aux.Substring (0, aux.IndexOf ("@")));
			aux = aux.Substring ((aux.IndexOf ("@")) + 1, aux.Length - (aux.IndexOf ("@") + 1));
			if(ce.getVida() != oldLife){
				ce.initVida (oldLife-5);
				oldLife = ce.getVida();
			}
			
			
			float balaX = float.Parse (aux.Substring (0, aux.IndexOf ("@")));
			aux = aux.Substring ((aux.IndexOf ("@")) + 1, aux.Length - (aux.IndexOf ("@") + 1));
			ce.setXB (balaX);
			
			
			float balaY = float.Parse (aux.Substring (0,aux.Length));
			ce.setYB (balaY);
			if (shootOnce) {
				ce.setSShoot (true);
				shootOnce = false;
			}
			aux = "";
			
			
		}else{
			float corX = float.Parse (aux.Substring (0, aux.IndexOf ("@")));
			aux = aux.Substring ((aux.IndexOf ("@")) + 1, aux.Length - (aux.IndexOf ("@") + 1));
			ce.setX (corX);
			
			
			float corY = float.Parse (aux.Substring (0, aux.IndexOf ("@")));
			aux = aux.Substring ((aux.IndexOf ("@")) + 1, aux.Length - (aux.IndexOf ("@") + 1));
			ce.setY (corY);
			
			
			float corA = float.Parse (aux.Substring (0, aux.IndexOf ("@")));
			aux = aux.Substring ((aux.IndexOf ("@")) + 1, aux.Length - (aux.IndexOf ("@") + 1));
			ce.setAngle(corA);
			
			
			float life = float.Parse (aux.Substring (0, aux.IndexOf ("@")));
			aux = aux.Substring ((aux.IndexOf ("@")) + 1, aux.Length - (aux.IndexOf ("@") + 1));
			if(ce.getVida() != oldLife){
				ce.initVida (oldLife-5);
				oldLife = ce.getVida();
			}
			
			
			String en = aux.Substring (0, aux.Length);
			shootOnce = true;
			aux ="";
			
			
			
			
		}
	}

	// solo cuenta la cantidad de arrobas
	int contador(String aux){
		int contador =0, posicion=0;
		String patron="@";
		for (int i=0; i<aux.Length;i++)
		{
			posicion=aux.IndexOf(patron);
			if (posicion!=-1)
			{
				contador++;
				aux=aux.Substring(posicion+patron.Length);
			}
		}
		return contador;
	}
}