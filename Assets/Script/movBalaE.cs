﻿/*Autor: Gerson Ivan Perez Dominguez
	12002764*/
using UnityEngine;
using System.Collections;

public class movBalaE : MonoBehaviour {
	public Transform tanque;
	// Use this for initialization
	coorEne cm;
	coorM co;
	Vector3 A;
	float tiempo;
	private AudioSource balaSource;
	public AudioClip bala,golpe,muerte;
	void Start () {
		//transform.rotation = Quaternion.Euler (0f, 0f,tanque.eulerAngles.z);
		cm = new coorEne ();
		co = new coorM ();
		//transform.position = new Vector3 (cm.getX(), cm.getY (), 0f);
		if (cm.isjefe ()) {
		//	transform.Rotate (new Vector3 (-180f, -180f, cm.getAngle ()));
			//A = Quaternion.Euler (0, 0, cm.getAngle ()) * Vector3.left * Time.deltaTime * 5;
			A = Quaternion.Euler (0, 0, cm.getAngle ()) * Vector3.left * 0.1f;
		} else {
			//transform.Rotate (new Vector3(0f,0f,cm.getAngle()));
			//A = Quaternion.Euler (0, 0, cm.getAngle ()) * Vector3.right * Time.deltaTime * 5;
			A = Quaternion.Euler (0, 0, cm.getAngle ()) * Vector3.right * 0.1f;
		}
		tiempo = Time.time  +3.5f;
		balaSource = GetComponent<AudioSource>();
		balaSource.clip = bala;
		balaSource.Play ();
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += A;
		if (tiempo < Time.time) {
			cm.setShoot(true);
			cm.setSShoot(false);
			Destroy (gameObject);
		}
	}

	void OnCollisionEnter2D (Collision2D  col)
	{
		if (col.gameObject.name == "tanque(Clone)") {
			co.setVida (5);
			if(co.getVida() !=0){
				AudioSource.PlayClipAtPoint(golpe,new Vector3(0f,0f,0f));
			}else{
				AudioSource.PlayClipAtPoint(muerte,new Vector3(0f,0f,0f));
			}
			cm.setShoot(true);
			cm.setSShoot(false);
			Destroy (gameObject);
		}else if (col.gameObject.name == "casa(Clone)") {
			cm.setShoot(true);
			cm.setSShoot(false);
			AudioSource.PlayClipAtPoint (golpe, new Vector3 (0f, 0f, 0f));
			Destroy (gameObject);
			
		}
	}
	
}