﻿/*Autor: Gerson Ivan Perez Dominguez
	12002764*/
using UnityEngine;
using System.Collections;

public class movBala : MonoBehaviour {
	public Transform tanque;
	// Use this for initialization
	coorM cm;
	coorEne ce;
	Vector3 A;
	float tiempo;
	private AudioSource balaSource;
	public AudioClip bala,recarga,golpe,muerte;
	void Start () {
		//transform.rotation = Quaternion.Euler (0f, 0f,tanque.eulerAngles.z);
		cm = new coorM ();
		ce = new coorEne ();
		//transform.position = tanque.transform.position + tanque.transform.right * 1.2f;
		if (ce.isjefe()) {//esto
			//transform.Rotate (new Vector3(0f,0f,cm.getAngle()));
			//A = Quaternion.Euler (0, 0, cm.getAngle ()) * Vector3.right * Time.deltaTime * 5;
			A = Quaternion.Euler (0, 0, cm.getAngle ()) * Vector3.right * 0.1f;
		} else {
			//transform.Rotate (new Vector3(-180f,-180f,cm.getAngle()));
			//A = Quaternion.Euler (0, 0, cm.getAngle ()) * Vector3.left * Time.deltaTime * 5;
			A = Quaternion.Euler (0, 0, cm.getAngle ()) * Vector3.left * 0.1f;
		}
		cm.setXB (transform.position.x);
		cm.setYB (transform.position.y);
		tiempo = Time.time  +3.5f;
		balaSource = GetComponent<AudioSource>();
		balaSource.clip = bala;
		balaSource.Play ();
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += A;
		cm.setXB (transform.position.x);
		cm.setYB (transform.position.y);
		if (tiempo < Time.time) {
			cm.setShoot(true);
			cm.setSShoot(false);
			AudioSource.PlayClipAtPoint(recarga,new Vector3(0f,0f,0f));
			Destroy (gameObject);
		}
	}

	void OnCollisionEnter2D (Collision2D  col)
	{
		if (col.gameObject.name == "tanqueE(Clone)") {
			ce.setVida (5);
			cm.setShoot (true);
			cm.setSShoot (false);
			if (ce.getVida () != 0) {
				AudioSource.PlayClipAtPoint (golpe, new Vector3 (0f, 0f, 0f));
			} else {
				AudioSource.PlayClipAtPoint (muerte, new Vector3 (0f, 0f, 0f));
			}
			AudioSource.PlayClipAtPoint (recarga, new Vector3 (0f, 0f, 0f));
			Destroy (gameObject);
		} else if (col.gameObject.name == "casa(Clone)") {
			cm.setShoot (true);
			cm.setSShoot (false);
			AudioSource.PlayClipAtPoint (golpe, new Vector3 (0f, 0f, 0f));
			Destroy (gameObject);

		}
	}
}
