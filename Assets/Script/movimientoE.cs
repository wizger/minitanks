﻿/*Autor: Gerson Ivan Perez Dominguez
	12002764*/
using UnityEngine;
using System.Collections;

public class movimientoE : MonoBehaviour {
	coorEne co;
	float oldX;
	float oldY;
	private AudioSource tanqueSource;
	public AudioClip tanque;
	public Transform bala,tanquePref;
	public Sprite herido,muerto,heridoN,muertoN;
	void Start () {
		co = new coorEne ();
		oldX = co.getX ();
		oldY = co.getY ();
		tanqueSource = GetComponent<AudioSource>();
		tanqueSource.clip = tanque;
	}
	
	// Update is called once per frame
	void Update () {
		//print (Camera.main.ScreenToWorldPoint(new Vector3 (co.getX()*Time.deltaTime,co.getY()*Time.deltaTime,0f)));
		//transform.Translate (Camera.main.ScreenToWorldPoint(new Vector3 (co.getX()*Time.deltaTime,co.getY()*Time.deltaTime,0f)));
		//transform.Translate (new Vector3 (0f,0f,0f));
		//print (co.getX ());
		//transform.position = (Camera.main.ScreenToWorldPoint(new Vector3 (co.getX()*Time.deltaTime,co.getY()*Time.deltaTime,0f)));
		//print (co.isjefe ());
		if (co.getVida () == 5) {
			if(co.isjefe()){
				tanquePref.GetComponent<SpriteRenderer>().sprite = heridoN;
			}else{
				tanquePref.GetComponent<SpriteRenderer>().sprite = herido;
			}
		}
		if (co.getVida () > 0) {
			movJe ();
		} else {
			if(co.isjefe()){
				tanquePref.GetComponent<SpriteRenderer>().sprite = muertoN;
			}else{
				tanquePref.GetComponent<SpriteRenderer>().sprite = muerto;
			}
		}
	}


	public void movJe(){
		transform.rotation = Quaternion.Euler (0f, 0f, co.getAngle ());
		if (co.canSShoot ()) {
			if(co.isjefe()){
				bala.transform.eulerAngles = new Vector3 (-180f,-180f,co.getAngle());
				Instantiate (bala,transform.position + transform.right * -1.2f,bala.transform.rotation);
			}else{
				bala.transform.eulerAngles = new Vector3 (0f,0f,co.getAngle());
				Instantiate (bala,transform.position + transform.right * 1.2f,bala.transform.rotation);
			}
			co.setSShoot(false);
		}
		if (oldX != co.getX ()) {
			if (!tanqueSource.isPlaying) {
				tanqueSource.Play ();
			}
			if (oldX < co.getX ()) {
				//transform.Translate (Vector3.right*Time.deltaTime);
				transform.position = new Vector3 (co.getX (), transform.position.y);
			} else {
				//transform.Translate (Vector3.left*Time.deltaTime);
				transform.position = new Vector3 (co.getX (), transform.position.y);
			}
			oldX = co.getX ();
		} else {

		}

		if (oldY != co.getY ()) {
			if (oldY < co.getY ()) {
				if (!tanqueSource.isPlaying) {
					tanqueSource.Play ();
				}
				//transform.Translate (Vector3.up*Time.deltaTime);
				transform.position = new Vector3 (transform.position.x, co.getY ());
			} else {
				//transform.Translate (Vector3.down*Time.deltaTime);
				transform.position = new Vector3 (transform.position.x, co.getY ());
			}
			oldY = co.getY ();
		} else {
			tanqueSource.Stop();
		}
	}
}
