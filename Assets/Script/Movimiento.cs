﻿/*Autor: Gerson Ivan Perez Dominguez
	12002764*/
using UnityEngine;
using System.Collections;

public class Movimiento : MonoBehaviour {
	coorM cm;
	coorEne ce;
	private AudioSource tanqueSource;
	public AudioClip tanque;
	public Transform bala,tanquePref;
	float tiempo;
	public Sprite herido,muerto,heridoN,muertoN;
	bool final;
	float tF;
	// Use this for initialization
	void Start () {
		cm = new coorM ();
		ce = new coorEne ();
		tanqueSource = GetComponent<AudioSource>();
		tanqueSource.clip = tanque;
		tiempo = Time.time;
		final = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (final) {
			if ((cm.getVida() <= 0) || (ce.getVida () <= 0)) {
				tF = Time.time + 5;	
				final = false;
			}
		} else {
			if(tF< Time.time){
				Application.LoadLevel ("final"); 
			}
		}

		if (cm.getVida () == 5) {
			if(ce.isjefe()){
				tanquePref.GetComponent<SpriteRenderer>().sprite = heridoN;
			}else{
				tanquePref.GetComponent<SpriteRenderer>().sprite = herido;
			}
		}
		if (cm.getVida () > 0) {
			if (Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D)) {
				transform.Translate (Vector3.right * Time.deltaTime);
				cm.setMove (false);
				if(!tanqueSource.isPlaying){
					tanqueSource.Play();
				}
			} else if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A)) {
					transform.Translate (Vector3.left * Time.deltaTime);
				if(!tanqueSource.isPlaying){
					tanqueSource.Play();
				}
			} else if (Input.GetKey (KeyCode.UpArrow) || Input.GetKey (KeyCode.W)) {
				if(!tanqueSource.isPlaying){
					tanqueSource.Play();
				}
				if (ce.isjefe ()) {
					transform.Rotate (Vector3.forward * 0.8f);
				} else {
					transform.Rotate (Vector3.forward * -0.8f);
				}
				cm.setAngle (transform.eulerAngles.z);
				transform.rotation = Quaternion.Euler (0f, 0f, cm.getAngle ());
			} else if (Input.GetKey (KeyCode.DownArrow) || Input.GetKey (KeyCode.S)) {
				if(!tanqueSource.isPlaying){
					tanqueSource.Play();
				}
				if (ce.isjefe ()) {
					transform.Rotate (Vector3.forward * -0.8f);
				} else {
					transform.Rotate (Vector3.forward * 0.8f);
				}
				cm.setAngle (transform.eulerAngles.z);
				transform.rotation = Quaternion.Euler (0f, 0f, cm.getAngle ());
			} else if (Input.GetKey (KeyCode.Space)) {
				if(tiempo < Time.time){
					if(cm.canShoot()){
						tiempo = Time.time + 4.3f;
						if(ce.isjefe()){
							bala.transform.eulerAngles = new Vector3 (0f,0f,cm.getAngle());
							Instantiate (bala,transform.position + transform.right * 1.2f,bala.transform.rotation);
						}else{
							bala.transform.eulerAngles = new Vector3 (-180f,-180f,cm.getAngle());
							Instantiate (bala,transform.position + transform.right * -1.2f,bala.transform.rotation);
						}
						cm.setShoot(false);
						cm.setSShoot(true);
					}
				}
			}else {
				tanqueSource.Stop();
			}
		} else {
			if(ce.isjefe()){
				tanquePref.GetComponent<SpriteRenderer>().sprite = muertoN;
			}else{
				tanquePref.GetComponent<SpriteRenderer>().sprite = muerto;
			}
		}

		cm.setAngle (transform.eulerAngles.z);
		cm.setXY(transform.position.x,transform.position.y);
	}
	
}
