/*Autor: Gerson Ivan Perez Dominguez
	12002764*/
using UnityEngine;
using System.Collections;

public class coorM : MonoBehaviour {
	static float X,XB;
	static float Y,YB;
	static bool move;
	static float angle;
	static bool shoot;
	static bool Sshoot;
	static int vida;
	public void setXY(float x1,float y1){
		X = x1;
		Y = y1;
	}

	public void initVida(int v){
		vida = v;
	}

	public void setVida(int v){
		vida -= v;
	}

	public int getVida(){
		return vida;
	}
	public void setX(float x1){
		X = x1;
	}
	
	public void setY(float y1){
		Y = y1;
	}

	public void setXB(float x1){
		XB = x1;
	}
	
	public void setYB(float y1){
		YB = y1;
	}
	
	public float getX(){
		return X;
	}
	
	public float getY(){
		return Y;
	}

	public float getXB(){
		return XB;
	}
	
	public float getYB(){
		return YB;
	}

	public void setMove(bool m){
		move = m;
	}

	public bool canMove(){
		return move;
	}

	public void setAngle(float an){
		angle = an;
	}

	public float getAngle(){
		return angle;
	}

	public void setShoot(bool s){
		shoot = s;
	}

	public bool canShoot(){
		return shoot;
	}

	public void setSShoot(bool s){
		Sshoot = s;
	}
	
	public bool canSShoot(){
		return Sshoot;
	}

	public void restart(){
		X = XB = Y = YB = angle = 0f;
		move = shoot = Sshoot = false;
		vida = 0;
	}

}
